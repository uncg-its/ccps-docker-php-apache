# .bashrc

# User specific aliases and functions

alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'
alias exportproxy='export https_proxy="proxy.uncg.edu:3128" && export http_proxy="proxy.uncg.edu:3128" && export HTTP_PROXY="proxy.uncg.edu:3128" && export HTTPS_PROXY="proxy.uncg.edu:3128"'
alias ll='ls -lah'

if [ -e /usr/share/terminfo/x/xterm+256color ]; then
  export TERM='xterm-256color'
else
  export TERM='xterm-color'
fi

# Laravel and Development
alias art='php artisan'
alias tink='php artisan tinker'
alias dump='composer dump'
alias dumps='php artisan dump-server'
alias sup='supervisorctl'
