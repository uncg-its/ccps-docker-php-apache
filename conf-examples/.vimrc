syntax on
set backspace=2
set showcmd
set nocursorline
set ignorecase
set hlsearch
set number
set incsearch