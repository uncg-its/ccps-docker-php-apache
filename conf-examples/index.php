<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>UNCG ITS - Cloud Collaboration &amp; Productivity Services</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 36px;
                padding: 20px;
            }
        a {
        text-decoration: none;
        color: #636b6f;
        }
        a:hover {
        text-decoration: underline;
        }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title">
                    <p>UNCG ITS<br />
            Cloud Collaboration &amp; Productivity Services<br />
            </p>
            <p>
            <a href="http://6tech.uncg.edu" title="6-TECH Website">6tech.uncg.edu</a>
            </p>
                </div>
            </div>
        </div>
    </body>
</html>
