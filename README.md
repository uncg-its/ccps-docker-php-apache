# Welcome
Repository for the CCPS group's work on Docker containers for PHP & Apache.

The intent is to use this container to replace the packaged versions of PHP & Apache available in RHEL7.

This should also allow the CCPS group to maintain consistency across local/dev/val/prod environments by keeping version numbers in sync.


# Getting Started
(Standard Makefile usage is followed. View /Makefile for all available options)
```sh
$ more Makefile
```
## PHP.INI
Create your php.ini file (make a new one, or use the conf-examples/php.ini as a template). This needs to happen before building the image, as the file is copied into the container (not at runtime).
```sh
$ cp conf-examples/php.ini mounted-volumes/conf/php/php.ini
```

## BASH + VIM
Create your .bashrc and .vimrc file (make a new one, or use the conf-examples/.vimrc & conf-examples/.bashrc as templates). This needs to happen before building the image, as the file is copied into the container (not at runtime).
```sh
$ cp conf-examples/.vimrc mounted-volumes/conf/vim/.vimrc
$ cp conf-examples/.bashrc mounted-volumes/conf/bash/.bashrc
```

## CRON
Create a crontab.txt and place into the /mounted-volumes/conf/cron directory, or use the conf-examples/crontab.txt as a template).
Note that this will only be pulled into the container at *docker build* time, not *docker run* time.
```sh
$ cp conf-examples/crontab.txt mounted-volumes/conf/cron/crontab.txt
```
This will make sure the cron directive in the build command below will work correctly.
If you need to add/alter the crontab in between builds, or without restarting the container, then you will need to login to the container, edit the crontab, and then also make sure your changes are saved in the mounted-volumes/conf/cron/crontab.txt as well (or your changes would be lost on the next rebuild)

Note: Make sure that you have a newline at the end of the file, or you will get this error:
```sh
  new crontab file is missing newline before EOF, can't install.
  The command '/bin/sh -c crontab /root/crontab/crontab.txt' returned a non-zero code: 1
```

## Supervisor
Supervisor should run automatically at each restart of the container. It can also be run manually with the `supervisord` command at the command line.

Supervisor manages both `cron` and `apache` processes.

You will need to copy the `conf-examples/supervisord.conf` file into `mounted-volumes/conf/supervisor` before building the image.

```sh
$ cp conf-examples/supervisord.conf mounted-volumes/conf/supervisor/supervisord.conf
```

More information on using Supervisor for Laravel applications below. This is all that is necessary for building the image.

## Logrotate
In the event that some processes generate non-rotating logs (for instance, Laravel Horizon), the `logrotate` package has been added to this image to handle the rotation of logs. A sample config file is provided at `conf-examples/logrotate-horizon` to rotate all `horizon.log` files in the `/var/www/apps/*/` folder(s).

To use this feature:

- copy the sample file `conf-examples/logrotate-horizon` to `mounted-volumes/conf/logrotate` (use a different name if you wish) - and edit it to perform as you want (docs [here](https://manpages.debian.org/stretch/logrotate/logrotate.8.en.html)). Logrotate is configured to read all log files in that folder, so you can either add more files or augment that existing file to cover other needs you may discover over time.
- You can run this manually on the command line by running `logrotate /etc/logrotate.conf` - there is a sample crontab entry included in `conf-examples/crontab.txt` - or you can find another way to run this command on a schedule (e.g. Laravel's scheduler).

## BUILD IMAGE
Build the image:
```sh
$ make build-image-no-proxy // for local development
OR
$ make build-image-with-proxy // for remote deployment behind proxy
```
## DOCKER NETWORK
Create the Docker Network for this instance to run on

Note: you only need to do this if the network isn't already created (ie: if you're also using the PHP/Apache CCPS instance)
```sh
$ make create-network
```
## RUN/START CONTAINER
Create/run an instance based on that image:
```sh
$ make run-instance
OR
$ make run-instance ENV=prod
OR
$ make run-instance ENV=prod PROXY=true
```
## DEPLOY APP
Create your app directory in the mounted-volumes/html directory

If you need to run composer to install your app, you may need to do that from inside the instance.
```sh
$ make login-to-instance
$ cd /var/www/html/myApp
$ composer install
```

That's it.

# Other Stuff

## SSH

### Keys
If you need to use SSH keys for your server, the `mounted-volumes/conf/ssh` directory will be a persistent volume that you can use to store the keys, that gets mounted to `/root/.ssh` inside the container.

### Configuration
The included `conf-examples/ssh-proxy` file is an example ssh config file, and its contents should be added to your `mounted-volumes/conf/ssh/config` file. This will ensure that `socat` is used to send the request through the proxy.

### Git configuration
Per Git documentation, a Git repository can be told which SSH key to use during its commands with the `core.sshcommand` config option. This can be set per-repo (recommended) rather than using the `IdentityFile` directive in the ssh config file.

To do so, use:
```sh
git config core.sshcommand "ssh -i ~/.ssh/MY-KEY-NAME-HERE"
```

Then, just be sure that your remote is using the `git` protocol (`git@bitbucket.org`) vs the `https` protocol (`https://bitbucket.org`)

If cloning a new repo, you can simply use the runtime `-c` flag to set the config, as below:

```sh
git -c core.sshcommand="ssh -i ~/.ssh/MY-KEY" clone git@bitbucket.org:uncg-its/app-that-uses-MY-KEY.git my-app
```

## SSL
SSL Support / Self-Signed Cert (run from root path of this container)
```sh
$ openssl req -x509 -nodes -days 3650 -newkey rsa:2048 -keyout mounted-volumes/conf/ssl/keys/ccps-wildcard.key -out mounted-volumes/conf/apache/ccps-wildcard.crt
```
(Answer prompts, making sure to put *.localhost as the FQDN when asked).

Add VirtualHost block:
```
<VirtualHost *:443>
    DocumentRoot /var/www/html/ccps-appname/public
    ServerName ccps-appname.localhost
    SSLEngine on
    SSLCertificateFile /etc/apache2/sites-enabled/ccps-wildcard.crt
    SSLCertificateKeyFile /etc/ssl/keys/ccps-wildcard.key
    SSLProtocol All -SSLv2 -SSLv3
    SSLCipherSuite "EECDH+ECDSA+AESGCM EECDH+aRSA+AESGCM EECDH+ECDSA+SHA384 EECDH+ECDSA+SHA256 EECDH+aRSA+SHA384 EECDH+aRSA+SHA256 EECDH+aRSA+RC4 EECDH EDH+aRSA RC4 !aNULL !eNULL !LOW !3DES !MD5 !EXP !PSK !SRP !DSS !RC4"
    SSLHonorCipherOrder on
    AllowEncodedSlashes On
</VirtualHost>

```
Save Changes, then Restart container (make restart-instance) and visit https://ccps-appname.localhost in browser

## More Supervisor

Supervisor runs the core `cron` and `apache` services, but also should be used to run queues for Laravel apps.

A sample configuration file for a Laravel application can be found in the `conf-examples` folder. Consult [Laravel documentation](https://laravel.com/docs/7.x/queues#supervisor-configuration) for more information on configuring and starting processes with Supervisor for your application.

> NOTE: Supervisor WILL RETAIN running process groups across restarts. Once you initialize your process group the first time (`supervisorctl start xxx`), you should only need to restart it if you had manually stopped it.

The `mounted-volumes/conf/supervisor` folder in this repo is mounted to `/etc/supervisor` in the container, and so if you place any custom `.conf` files into `mounted-volumes/conf/supervisor/conf.d`, they will persist across stop/start/rebuild actions.

> If using Redis + Laravel Horizon, note that you should consult their help documentation for a recommended supervisor config file.

## AutoMySQL Backup
Create then edit the variables as needed in the automysqlbackup.conf file
```sh
$ cd {DEPLOY_ROOT_PATH}/ccps-docker-php-apache
$ cp mounted-volumes/scripts/db-backup/automysqlbackup.example.conf mounted-volumes/scripts/db-backup/automysqlbackup.conf
$ vi mounted-volumes/scripts/db-backup/automysqlbackup.conf
```
Create a user for the AutoMySQLBackup script to use
 - Needs global privs for SELECT, SHOW VIEWS, LOCK TABLES
 - Make sure it can connect to the container (% or specific hostname for the instance-php-apache container)

## Composer

Composer settings are standard, out of the box. However, the `/root/composer` volume is mapped outside the container, so that any stored OAuth information is carried over between restarts. This is mapped into `mounted-volumes/conf/composer`.

# Tagged Version Details

## 1.11.1

- proxy configuration is now separated from the maintenance scripts

## 1.11.0

- separate out proxy configuration from ENV

## 1.10.0

- add mapping for `/root/composer` so that OAuth creds aren't lost when container is rebuilt
- FROM: php8.0-apache

## 1.9.0
- remove `redis` PHP extension for backward-compatibility concerns
- adds `sockets` PHP extension
- add ENV check for mount method of `html` and `apps` directories, so that we can use `delegated` to improve performance on Docker for Mac

## 1.8.1
- add `redis` PHP extension for native interfacing with Redis

## 1.8.0
- adds pruning to maintenance scripts (non-dev)
- correct `socat` command in ssh-proxy example file
- adds `/root/.ssh` folder during build
- adds `make cleanup` to help automatically clean up old docker images
- adding some mirrors for `debian.org` in case main is blocked by UNCG networks
- update README links to latest version of Laravel

## 1.7.0
- FROM: php:7.4-apache
- tweak build dependencies and GD config consistent with PHP 7.4 requirements
- add `socat` for using SSH over proxy
- add `.ssh` directory mount for SSH keys

## 1.6.1
- Explicitly calling `rebuild` during the automated teardown / rebuild scripts, and ensuring that the `--pull` flag is used so that latest version is used

## 1.6.0
- FROM php:7.3-apache again, now that more versions have been released past security fix

## 1.5.0
- Explicit PHP 7.3.8 version (security fix from older PHP 7.3.x)
- implement `.dockerignore` to improve build context drastically

## 1.4.1
- logrotate
- new `ll` Bash alias (`ls -lah`) in sample config

## 1.4.0
- FROM php:7.3-apache
- add libzip-dev and configure installation of zip package in accordance with PHP 7.3 requirements
- add aliases for common PHP / Laravel commands to `.bashrc`

## 1.3.1
- add zip and unzip extensions to suppress Composer warnings

## 1.3.0
- FROM php:7.2-apache
- add pcntl php extension for Laravel Horizon
- add Supervisor
- place Supervisor in control of Cron and Apache services

## 1.2.7
- FROM php:7.2-apache
- add bcmath php extension for Laravel Telescope support

## 1.2.6
- FROM php:7.2-apache
- make checkout-latest-tag included to make it easier to upgrade

## 1.2.5
- FROM php:7.2-apache
- Added sudo for use inside container
- Changed crontab example to include sudo, fixing permissions problems for laravel apps writing the log files as root

## 1.2.4
- FROM php:7.2-apache
- fixed bug with logging in prod environment (now goes to correct /var/log/docker/instance-php-apache/)
- default UNCG proxy support added when running ENV=PROD

## 1.2.3
- FROM php:7.2-apache
- Cron and Apache services now run automatically on host reboot
- Removal of cron/apache restart-services commands in Makefile
- Date written to file example in crontab.txt for testing

## 1.2.2
- FROM php:7.2-apache
- adds Cron support
- AutoMySQLBackup Script included

## 1.2.1
- FROM php:7.2-apache
- adds gnupg for installation of google-chrome-stable
- re-adding chrome stuff and wget for Dusk support

## 1.2
- FROM php:7.2-apache
- Removed mcrypt (no longer required/dependency)

## 1.0 and 1.1
- FROM php:7.1.8-apache
