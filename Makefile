# Cloud Collaboration & Productivity Services, ITS, University of North Carolina at Greensboro

#########################
#						#
#   USAGE				#
#						#
#########################

# Use like any regular *nix Makefile.
# syntax: make <command>
# Example: make build-image-no-proxy

#########################
#						#
#   MAKEFILE VARS		#
#						#
#########################

ENV ?= dev
PROXY ?= false
PROXY_VALUE =

ifeq ($(ENV),dev)
      HOST_PATH_TO_SSL_KEYS = $(CURDIR)/mounted-volumes/conf/ssl/keys
      HOST_PATH_TO_LOGS ?= $(CURDIR)/mounted-volumes/log/instance-php-apache
	  MOUNT=delegated
else
      HOST_PATH_TO_SSL_KEYS = /etc/ssl/keys
      HOST_PATH_TO_LOGS = /var/log/docker/instance-php-apache
	  MOUNT=Z
endif

ifeq ($(PROXY),true)
      PROXY_VALUE = -e https_proxy=proxy.uncg.edu:3128 -e http_proxy=proxy.uncg.edu:3128 -e HTTP_PROXY=proxy.uncg.edu:3128 -e HTTPS_PROXY=proxy.uncg.edu:3128
endif


echo-path-to-ssl-keys:
	echo $(HOST_PATH_TO_SSL_KEYS)


#########################
#						#
#   DOCKER SPECIFICS	#
#						#
#########################

view-instance-logs:
	docker logs instance-php-apache

get-instance-ip:
	docker inspect instance-php-apache | grep "IPAddress"

build-image-no-proxy:
	docker build -t img-php-apache .

build-image-with-proxy:
	docker build --build-arg http_proxy=http://proxy.uncg.edu:3128 --build-arg https_proxy=http://proxy.uncg.edu:3128 --build-arg HTTP_PROXY=http://proxy.uncg.edu:3128 --build-arg HTTPS_PROXY=http://proxy.uncg.edu:3128 -t img-php-apache .

rebuild-image-no-proxy:
	docker build --pull --no-cache -t img-php-apache .

rebuild-image-with-proxy:
	docker build --pull --no-cache  --build-arg http_proxy=http://proxy.uncg.edu:3128 --build-arg https_proxy=http://proxy.uncg.edu:3128 --build-arg HTTP_PROXY=http://proxy.uncg.edu:3128 --build-arg HTTPS_PROXY=http://proxy.uncg.edu:3128 -t img-php-apache .

remove-image:
	docker rmi img-php-apache

create-network:
	docker network create --driver bridge ccps_docker_network

# NOTE: instance may need to be restarted again if the local folders do not exist already before the first run.
run-instance:
	docker run -d \
	-p 80:80 \
	-p 443:443 \
	-e TZ=America/New_York \
	-v $(CURDIR)/mounted-volumes/html:/var/www/html/:$(MOUNT) \
	-v $(CURDIR)/mounted-volumes/apps:/var/www/apps/:$(MOUNT) \
	-v $(HOST_PATH_TO_LOGS)/apache2:/var/log/apache2/:Z \
	-v $(HOST_PATH_TO_LOGS)/cron:/var/log/cron/:Z \
	-v $(HOST_PATH_TO_LOGS)/supervisor:/var/log/supervisor/:Z \
	-v $(CURDIR)/mounted-volumes/scripts:/var/ccps-scripts/:Z \
	-v $(CURDIR)/mounted-volumes/conf/apache:/etc/apache2/sites-enabled:Z \
	-v $(CURDIR)/mounted-volumes/conf/supervisor:/etc/supervisor:Z \
	-v $(CURDIR)/mounted-volumes/conf/logrotate:/etc/logrotate.d:Z \
	-v $(CURDIR)/mounted-volumes/conf/ssh:/root/.ssh/:Z \
	-v $(HOST_PATH_TO_SSL_KEYS):/etc/ssl/keys:Z \
	-v $(CURDIR)/mounted-volumes/conf/composer:/root/composer/:Z \
	$(PROXY_VALUE) \
	--name instance-php-apache \
	--restart always \
	--network=ccps_docker_network \
	img-php-apache

restart-instance:
	docker restart instance-php-apache

stop-instance:
	docker stop instance-php-apache

stop-remove-instance:
	docker stop instance-php-apache
	docker rm instance-php-apache

login-to-instance:
	sudo docker exec -i -t instance-php-apache /bin/bash

#########################
#						#
#   MAINTENANCE SETS	#
#						#
#########################

script-checkout-latest-tag:
	$(CURDIR)/checkout-latest-tag.sh

script-prod-rebuild-tear-down-restart-no-proxy:
	make rebuild-image-no-proxy
	make stop-remove-instance
	make run-instance ENV=prod PROXY=$(PROXY)
	make prune-images
	make cleanup

script-dev-rebuild-tear-down-restart-no-proxy:
	make rebuild-image-no-proxy
	make stop-remove-instance
	make run-instance ENV=dev PROXY=$(PROXY)

script-prod-rebuild-tear-down-restart-with-proxy:
	make rebuild-image-with-proxy
	make stop-remove-instance
	make run-instance ENV=prod PROXY=$(PROXY)
	make prune-images
	make cleanup

script-dev-rebuild-tear-down-restart-with-proxy:
	make rebuild-image-with-proxy
	make stop-remove-instance
	make run-instance ENV=dev PROXY=$(PROXY)

prune-images:
	docker image prune --force

cleanup:
	- docker rm -v $(docker ps --filter status=exited -q 2>/dev/null)
	- docker rmi $(docker images --filter dangling=true -q 2>/dev/null)

#########################
#						#
#   UNCG HELPERS		#
#						#
#########################

export-uncg-proxy:
	export https_proxy="proxy.uncg.edu:3128" && export http_proxy="proxy.uncg.edu:3128" && export HTTP_PROXY="proxy.uncg.edu:3128" && export HTTPS_PROXY="proxy.uncg.edu:3128"
