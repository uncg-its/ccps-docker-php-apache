#!/bin/bash
# get all tags
git fetch --tags
# store latest tag in variable name
latesttag=$(git describe --tags)
# log latest tag checkout command to screen
echo checking out ${latesttag}
#checkout the latest tag
git checkout ${latesttag}