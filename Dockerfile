FROM php:8.0-apache
LABEL maintainer="its-laravel-devs-l@uncg.edu"

# Install PHP extensions and PECL modules.

RUN buildDeps=" \
    sudo \
    openssl \
    libbz2-dev \
    libmemcached-dev \
    default-libmysqlclient-dev \
    libsasl2-dev \
    libzip-dev \
    libonig-dev \
    " \
    runtimeDeps=" \
    curl \
    wget \
    gnupg \
    git \
    vim \
    libfreetype6-dev \
    libicu-dev \
    libjpeg-dev \
    libmemcachedutil2 \
    libpng-dev \
    libpq-dev \
    logrotate \
    default-mysql-client \
    unzip \
    zip \
    socat \
    " \
    && sh -c 'echo "deb [arch=amd64] http://ftp.us.debian.org/debian/ stable main" >> /etc/apt/sources.list.d/debian-us-mirror.list' \
    && sh -c 'echo "deb [arch=amd64] http://debian.csail.mit.edu/debian/ stable main" >> /etc/apt/sources.list.d/debian-us-mirror-mit.list' \
    && apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y $buildDeps $runtimeDeps

RUN \
    docker-php-ext-install bcmath bz2 calendar iconv intl mysqli opcache pcntl pdo_mysql pdo_pgsql pgsql sockets \
    && docker-php-ext-configure gd --enable-gd --with-freetype=/usr/include/ --with-jpeg=/usr/include/ \
    && docker-php-ext-install gd \
    && docker-php-ext-install zip \
    && apt-get purge -y --auto-remove $buildDeps \
    && rm -r /var/lib/apt/lists/* \
    && a2enmod rewrite \
    && a2enmod ssl

RUN \
    apt-get update && \
    apt-get install cron libldap2-dev openssl ca-certificates -y && \
    rm -rf /var/lib/apt/lists/* && \
    docker-php-ext-configure ldap --with-libdir=lib/x86_64-linux-gnu/ && \
    docker-php-ext-install ldap

RUN \
    wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - && \
    sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list' && \
    apt-get update && \
    apt-get install -y google-chrome-stable

COPY mounted-volumes/conf/php/php.ini /usr/local/etc/php/
COPY mounted-volumes/conf/vim/.vimrc /root/.vimrc
COPY mounted-volumes/conf/bash/.bashrc /root/.bashrc
RUN mkdir /root/crontab && mkdir /var/log/cron && mkdir /root/.ssh

# Install Composer.
ENV COMPOSER_HOME /root/composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
ENV PATH $COMPOSER_HOME/vendor/bin:$PATH

# Install Laravel Installer
RUN composer global require "laravel/installer" -q

# Run CRON service automatically
RUN touch /var/log/cron/cron.log
RUN touch /var/log/cron/datelog.txt
COPY mounted-volumes/conf/cron/crontab.txt /etc/cron.d/ccps-crontab
# Give execution rights on the cron job

# Install supervisor
RUN apt-get install -y supervisor

# Starting services automatically
COPY start-services.sh /root/start-services.sh
RUN chmod 0644 /root/start-services.sh
RUN chmod +x /root/start-services.sh
CMD ["/root/start-services.sh"]
