#!/bin/bash
#load crontab that was copied in during Dockerfile build
crontab /etc/cron.d/ccps-crontab
# Give execution rights on the crontab
chmod 0644 /etc/cron.d/ccps-crontab

#supervisor
/usr/bin/supervisord -nc /etc/supervisor/supervisord.conf &

# start services so they continue to run after reboot of host
touch /var/log/cron/cron.log && tail -f /var/log/cron/cron.log